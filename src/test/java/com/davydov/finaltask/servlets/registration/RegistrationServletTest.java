package com.davydov.finaltask.servlets.registration;

import com.davydov.finaltask.SqlTestBase;
import com.davydov.finaltask.service.Field;
import com.davydov.finaltask.service.registration.StudentCreateResponse;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.sql.Connection;

import static com.davydov.finaltask.servlets.registration.RegistrationField.*;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class RegistrationServletTest extends SqlTestBase {
    private static final String EMAIL_ARG = "dmytro.davydov@gmail.com";
    private static final String PASSWORD_ARG = "password";
    private static final String PASSWORD_ARG_SPACES = "    ";

    private RegistrationServlet registrationServlet;

    @Mock
    private HttpServletRequest request;

    @Captor
    private ArgumentCaptor<StudentCreateResponse> response;

    private static <T> void assertErrorField(String fieldName, Field<T> field, T value, String message) {
        assertEquals(fieldName, false, field.isValid());
        assertEquals(fieldName, value, field.getValue());
        assertEquals(fieldName, message, field.getError());
    }

    @Before
    public void setUp() throws Exception {
        registrationServlet = new RegistrationServlet();
        registrationServlet.init();

        when(request.getParameter(EMAIL.name())).thenReturn(EMAIL_ARG);
        when(request.getParameter(PASSWORD.name())).thenReturn(PASSWORD_ARG);
        when(request.getParameter(PASSWORD_RETRY.name())).thenReturn(PASSWORD_ARG);
    }

    @Test
    public void shouldNotPassRequestWhenEmailIsNull() throws ServletException, IOException {
        when(request.getParameter(EMAIL.name())).thenReturn(null);

        registrationServlet.doPost(request, null);

        StudentCreateResponse student = getStudent(response);
        assertErrorField("email", student.getEmail(), null, "Email should be specified");
    }

    @Test
    public void shouldNotPassRequestWhenEmailAlreadyExist() throws ServletException, IOException {
        when(request.getParameter(EMAIL.name())).thenReturn(null);

        registrationServlet.doPost(request, null);

        StudentCreateResponse student = getStudent(response);
        assertErrorField("email", student.getEmail(), null, "Email should be specified");
    }

    @Test
    public void shouldNotPassRequestWhenEmailIsEmpty() throws ServletException, IOException {
        when(request.getParameter(EMAIL.name())).thenReturn("");

        registrationServlet.doPost(request, null);

        StudentCreateResponse student = getStudent(response);
        assertErrorField("email", student.getEmail(), "", "Email should be specified");
    }

    @Test
    public void shouldNotPassRequestWhenEmailContainsOnlySpaces() throws ServletException, IOException {
        when(request.getParameter(EMAIL.name())).thenReturn(PASSWORD_ARG_SPACES);

        registrationServlet.doPost(request, null);

        verify(request).setAttribute(eq("student"), response.capture());

        StudentCreateResponse student = getStudent(response);
        assertErrorField("email", student.getEmail(), PASSWORD_ARG_SPACES, "Email should be specified");
    }

    @Test
    public void shouldNotPassRequestWhenPasswordIsNull() throws ServletException, IOException {
        when(request.getParameter(PASSWORD.name())).thenReturn(null);

        registrationServlet.doPost(request, null);

        StudentCreateResponse student = getStudent(response);
        assertErrorField("email", student.getPassword(), null, "Password should be specified");
    }

    @Test
    public void shouldNotPassRequestWhenPasswordIsEmpty() throws ServletException, IOException {
        when(request.getParameter(PASSWORD.name())).thenReturn("");
        when(request.getParameter(PASSWORD_RETRY.name())).thenReturn("");

        registrationServlet.doPost(request, null);

        StudentCreateResponse student = getStudent(response);
        assertErrorField("password", student.getPassword(), "", "Password should be specified");
    }

    @Test
    public void shouldNotPassRequestWhenPasswordContainsOnlySpaces() throws ServletException, IOException {
        when(request.getParameter(PASSWORD.name())).thenReturn(PASSWORD_ARG_SPACES);
        when(request.getParameter(PASSWORD_RETRY.name())).thenReturn(PASSWORD_ARG_SPACES);

        registrationServlet.doPost(request, null);

        StudentCreateResponse student = getStudent(response);
        assertErrorField("password", student.getPassword(), PASSWORD_ARG_SPACES, "Password should be specified");
    }

    @Test
    public void shouldNotPassRequestWithoutPasswordRetry() throws ServletException, IOException {
        when(request.getParameter(PASSWORD.name())).thenReturn(null);
        when(request.getParameter(PASSWORD_RETRY.name())).thenReturn(null);

        registrationServlet.doPost(request, null);

        StudentCreateResponse student = getStudent(response);
        assertErrorField("password", student.getPasswordRetry(), null, "Repeated password should be specified");
    }

    @Test
    public void shouldNotPassRequestWhenPasswordsNotMatch() throws ServletException, IOException {
        when(request.getParameter(PASSWORD_RETRY.name())).thenReturn("password2");

        registrationServlet.doPost(request, null);

        StudentCreateResponse student = getStudent(response);
        assertErrorField("passwords", student.getPassword(), PASSWORD_ARG, "");
        assertErrorField("passwords", student.getPasswordRetry(), "password2", "Passwords do not match");
    }

    @Test
    public void shouldNotPassRequestWhenEmailAlreadyRegistered() throws ServletException, IOException {
        registrationServlet.doPost(request, null);
        StudentCreateResponse student = getStudent(response);
        assertEquals("first student create", student.containsErrors(), false);
        reset(request);

        when(request.getParameter(EMAIL.name())).thenReturn(EMAIL_ARG);
        when(request.getParameter(PASSWORD.name())).thenReturn(PASSWORD_ARG + "1");
        when(request.getParameter(PASSWORD_RETRY.name())).thenReturn(PASSWORD_ARG + "1");

        registrationServlet.doPost(request, null);

        student = getStudent(response);
        assertErrorField("passwords", student.getEmail(), EMAIL_ARG, "This email already registered in system");
    }

    private StudentCreateResponse getStudent(ArgumentCaptor<StudentCreateResponse> response) {
        verify(request).setAttribute(eq("student"), response.capture());
        return response.getValue();
    }

    @After
    public void tearDown() throws Exception {
        try (Connection connection = getConnection()) {
            connection.createStatement().execute(
                    "DELETE FROM STUDENT WHERE EMAIL='" + EMAIL_ARG + "'"
            );
            connection.commit();
        }
    }
}