package com.davydov.finaltask.servlets.registration;

import com.davydov.finaltask.SqlTestBase;
import com.davydov.finaltask.dao.*;
import com.davydov.finaltask.dao.jdbc.student.StudentDAOFactory;
import com.davydov.finaltask.dao.jdbc.univercity.UniversityDAOFactory;
import com.davydov.finaltask.service.registration.StudentCreateResponse;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.http.HttpServletRequest;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import static com.davydov.finaltask.servlets.registration.RegistrationField.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DetailedRegistrationServletTest extends SqlTestBase {
	private static final String EMAIL_ARG = "ihor.davydov@gmail.com";
	private static final String NAME_ARG = "Ihor";
	private static final String LAST_NAME_ARG = "Davydov";
	private static final String PATRONYMIC_ARG = "Vladimirovych";
	private static final String UNIVERSITY_ARG = "National Aerospace University – Kharkiv Aviation Institute";
	private static final String PASSWORD_ARG = "password";

	private DetailedRegistrationServlet servlet;
	@Mock
	private HttpServletRequest request;
	@Captor
	private ArgumentCaptor<StudentCreateResponse> response;
	private StudentDAO studentDAO;
	private UniversityDAO universityDAO;
	private Student student;

	@Before
	public void setUp() throws Exception {
		servlet = new DetailedRegistrationServlet();
		servlet.init();

		universityDAO = UniversityDAOFactory.getUniversityDAO();

		studentDAO = StudentDAOFactory.getStudentDAO();
		student = new Student(EMAIL_ARG, PASSWORD_ARG);
		studentDAO.saveStudent(student);

		when(request.getParameter(ID.name())).thenReturn(student.getId().toString());
		when(request.getParameter(NAME.name())).thenReturn(NAME_ARG);
		when(request.getParameter(LAST_NAME.name())).thenReturn(LAST_NAME_ARG);
		when(request.getParameter(PATRONYMIC.name())).thenReturn(PATRONYMIC_ARG);
		when(request.getParameter(UNIVERSITY.name())).thenReturn(UNIVERSITY_ARG);
	}

	@Test
	public void shouldStoreStudentDetails() throws Exception {
		servlet.doPost(request, null);

		Student curStudent = studentDAO.findStudentById(student.getId());

		assertStudent(curStudent);
	}

	private void assertStudent(Student actualStudent) throws SQLException {
		assertNotNull("student", student);

		StudentDetail actualSD = actualStudent.getStudentDetail().get();

		assertEquals("id", student.getId(), actualSD.getId());
		assertEquals("name", NAME_ARG, actualSD.getName());
		assertEquals("lastName", LAST_NAME_ARG, actualSD.getLastName());
		assertEquals("patronymic", PATRONYMIC_ARG, actualSD.getPatronymic());
		assertEquals("universityId", universityDAO.findUniversityByName(UNIVERSITY_ARG).getId(), actualSD.getUniversityId());
	}

	@After
	public void tearDown() throws Exception {
		try (Connection connection = getConnection(); Statement statement = connection.createStatement()) {
			statement.execute("DELETE FROM STUDENT_DETAIL WHERE ID=" + student.getId());
			statement.execute("DELETE FROM STUDENT WHERE ID=" + student.getId());
		}
	}
}