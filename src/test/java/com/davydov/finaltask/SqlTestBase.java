package com.davydov.finaltask;

import com.davydov.finaltask.util.PropertyUtil;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class SqlTestBase {
	protected Connection getConnection() throws IOException, SQLException {
		String[] credentials = PropertyUtil.loadValues("liquibase/liquibase.properties", "url", "username", "password");
		return DriverManager.getConnection(credentials[0], credentials[1], credentials[2]);
	}
}
