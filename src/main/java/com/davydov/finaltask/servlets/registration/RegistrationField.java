package com.davydov.finaltask.servlets.registration;

public enum RegistrationField {
	ID,
	EMAIL,
	PASSWORD,
	PASSWORD_RETRY,
	NAME,
	LAST_NAME,
	PATRONYMIC,
	COUNTRY,
	REGION,
	CITY,
	UNIVERSITY
}
