package com.davydov.finaltask.servlets.registration;

import com.davydov.finaltask.service.RegistrationService;
import com.davydov.finaltask.service.registration.RegistrationServiceFactory;
import com.davydov.finaltask.service.registration.StudentCreateRequest;
import com.davydov.finaltask.service.registration.StudentCreateResponse;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;

import static com.davydov.finaltask.servlets.registration.RegistrationField.*;

public class RegistrationServlet extends HttpServlet {

	private RegistrationService registrationService;

	@Override
	public void init() throws ServletException {
		try {
			registrationService = RegistrationServiceFactory.getRegistrationService();
		} catch (IOException e) {
			throw new ServletException(e);
		}
	}

	@Override
	protected void doPost(HttpServletRequest httpRequest, HttpServletResponse httpResponse) throws ServletException {
		try {
			httpRequest.setCharacterEncoding("UTF-8");

			StudentCreateRequest request = new StudentCreateRequest(
					httpRequest.getParameter(EMAIL.name()),
					httpRequest.getParameter(PASSWORD.name()),
					httpRequest.getParameter(PASSWORD_RETRY.name())
			);

			StudentCreateResponse response = registrationService.createStudent(request);
			httpRequest.setAttribute("student", response);
		} catch (SQLException | UnsupportedEncodingException e) {
			throw new ServletException(e);
		}
	}
}
