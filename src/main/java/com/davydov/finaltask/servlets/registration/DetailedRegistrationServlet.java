package com.davydov.finaltask.servlets.registration;

import com.davydov.finaltask.service.RegistrationService;
import com.davydov.finaltask.service.registration.RegistrationServiceFactory;
import com.davydov.finaltask.service.registration.StudentDetailCreateRequest;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;

import static com.davydov.finaltask.servlets.registration.RegistrationField.*;

public class DetailedRegistrationServlet extends HttpServlet {

	private RegistrationService registrationService;

	@Override
	public void init() throws ServletException {
		try {
			registrationService = RegistrationServiceFactory.getRegistrationService();
		} catch (IOException e) {
			throw new ServletException(e);
		}
	}

	@Override
	protected void doPost(HttpServletRequest httpRequest, HttpServletResponse resp) throws ServletException {
		try {
			httpRequest.setCharacterEncoding("UTF-8");

			StudentDetailCreateRequest request = new StudentDetailCreateRequest(
					httpRequest.getParameter(ID.name()),
					httpRequest.getParameter(NAME.name()),
					httpRequest.getParameter(LAST_NAME.name()),
					httpRequest.getParameter(PATRONYMIC.name()),
					httpRequest.getParameter(COUNTRY.name()),
					httpRequest.getParameter(REGION.name()),
					httpRequest.getParameter(CITY.name()),
					httpRequest.getParameter(UNIVERSITY.name())
			);

			registrationService.createStudent(request);
		} catch (SQLException | UnsupportedEncodingException e) {
			throw new ServletException(e);
		}
	}
}
