package com.davydov.finaltask.service.registration;

import com.davydov.finaltask.service.Field;

public class StudentCreateResponse {
	private Field<String> email;
	private Field<String> password;
	private Field<String> passwordRetry;

	public StudentCreateResponse(String email, String password, String passwordRetry) {
		this.email = new Field<>(email);
		this.password = new Field<>(password);
		this.passwordRetry = new Field<>(passwordRetry);
	}

	public StudentCreateResponse() {
	}

	public Field<String> getEmail() {
		return email;
	}

	public void setEmail(Field<String> email) {
		this.email = email;
	}

	public Field<String> getPassword() {
		return password;
	}

	public void setPassword(Field<String> password) {
		this.password = password;
	}

	public Field<String> getPasswordRetry() {
		return passwordRetry;
	}

	public void setPasswordRetry(Field<String> passwordRetry) {
		this.passwordRetry = passwordRetry;
	}

	public boolean containsErrors() {
		return !(email.isValid() && password.isValid() && passwordRetry.isValid());
	}
}
