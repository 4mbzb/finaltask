package com.davydov.finaltask.service.registration;

import com.davydov.finaltask.dao.StudentDAO;
import com.davydov.finaltask.dao.UniversityDAO;
import com.davydov.finaltask.dao.jdbc.student.StudentDAOFactory;
import com.davydov.finaltask.dao.jdbc.univercity.UniversityDAOFactory;
import com.davydov.finaltask.service.RegistrationService;

import java.io.IOException;

public class RegistrationServiceFactory {
	public static RegistrationService getRegistrationService() throws IOException {
		StudentDAO studentDAO = StudentDAOFactory.getStudentDAO();
		UniversityDAO universityDAO = UniversityDAOFactory.getUniversityDAO();

		return new RegistrationProcessor(studentDAO, universityDAO);
	}
}
