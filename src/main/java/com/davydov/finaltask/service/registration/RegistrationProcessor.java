package com.davydov.finaltask.service.registration;

import com.davydov.finaltask.dao.*;
import com.davydov.finaltask.service.Field;
import com.davydov.finaltask.service.RegistrationService;

import java.sql.SQLException;
import java.util.Optional;

class RegistrationProcessor implements RegistrationService {

	private final StudentDAO studentDAO;
	private final UniversityDAO universityDAO;

	RegistrationProcessor(StudentDAO studentDAO, UniversityDAO universityDAO) {
		this.studentDAO = studentDAO;
		this.universityDAO = universityDAO;
	}

	public StudentCreateResponse createStudent(StudentCreateRequest request) throws SQLException {
		StudentCreateResponse response = new StudentCreateResponse();

		if (request.getEmail() == null || request.getEmail().trim().isEmpty())
			response.setEmail(new Field<>(request.getEmail(), "Email should be specified"));
		else
			response.setEmail(new Field<>(request.getEmail()));

		if (request.getPassword() == null || request.getPassword().trim().isEmpty())
			response.setPassword(new Field<>(request.getPassword(), "Password should be specified"));
		else
			response.setPassword(new Field<>(request.getPassword()));

		if (request.getPasswordRetry() == null || request.getPasswordRetry().trim().isEmpty())
			response.setPasswordRetry(new Field<>(request.getPasswordRetry(), "Repeated password should be specified"));
		else
			response.setPasswordRetry(new Field<>(request.getPasswordRetry()));

		if (request.getPassword() != null && !request.getPassword().equals(request.getPasswordRetry())) {
			response.setPassword(new Field<>(request.getPassword(), ""));
			response.setPasswordRetry(new Field<>(request.getPasswordRetry(), "Passwords do not match"));
		}

		Student existingStudent = studentDAO.findStudentByEmail(request.getEmail());
		if (existingStudent != null)
			response.setEmail(new Field<>(request.getEmail(), "This email already registered in system"));

		if (response.containsErrors())
			return response;

		Student student = convert(request);
		studentDAO.saveStudent(student);

		return response;
	}

	@Override
	public void createStudent(StudentDetailCreateRequest request) throws SQLException {
		Student student = studentDAO.findStudentById(Long.parseLong(request.getId()));

		University university = universityDAO.findUniversityByName(request.getUniversityName());

		student = fill(student, request, university);
		studentDAO.saveStudent(student);
	}

	private Student fill(Student student, StudentDetailCreateRequest request, University university) {
		StudentDetail studentDetail = new StudentDetail(
				student.getId(),
				request.getName(),
				request.getLastName(),
				request.getPatronymic(),
				university.getId()
		);

		student.setStudentDetail(Optional.of(studentDetail));

		return student;
	}

	private Student convert(StudentCreateRequest request) {
		return new Student(
				null,
				request.getEmail(),
				request.getPassword()
		);
	}

	private StudentCreateResponse convert(Student student) {
		return new StudentCreateResponse(
				student.getEmail(),
				student.getPassword(),
				student.getPassword()
		);
	}
}
