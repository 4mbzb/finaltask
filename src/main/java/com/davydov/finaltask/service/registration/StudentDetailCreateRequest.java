package com.davydov.finaltask.service.registration;

public class StudentDetailCreateRequest {
    private final String id;
    private final String name;
    private final String lastName;
    private final String patronymic;
    private final String country;
    private final String region;
    private final String city;
    private final String universityName;

    public StudentDetailCreateRequest(String id, String name, String lastName, String patronymic, String country, String region, String city, String university) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.patronymic = patronymic;
        this.country = country;
        this.region = region;
        this.city = city;
        this.universityName = university;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public String getCountry() {
        return country;
    }

    public String getRegion() {
        return region;
    }

    public String getCity() {
        return city;
    }

    public String getUniversityName() {
        return universityName;
    }
}
