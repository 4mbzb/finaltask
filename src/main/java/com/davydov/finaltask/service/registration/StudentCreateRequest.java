package com.davydov.finaltask.service.registration;

public class StudentCreateRequest {
	private final String email;
	private final String password;
	private final String passwordRetry;

	public StudentCreateRequest(String email, String password, String passwordRetry) {
		this.email = email;
		this.password = password;
		this.passwordRetry = passwordRetry;
	}

	public String getEmail() {
		return email;
	}

	public String getPassword() {
		return password;
	}

	public String getPasswordRetry() {
		return passwordRetry;
	}
}