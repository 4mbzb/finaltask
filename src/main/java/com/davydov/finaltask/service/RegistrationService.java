package com.davydov.finaltask.service;

import com.davydov.finaltask.service.registration.StudentCreateRequest;
import com.davydov.finaltask.service.registration.StudentCreateResponse;
import com.davydov.finaltask.service.registration.StudentDetailCreateRequest;

import java.sql.SQLException;

public interface RegistrationService {
	StudentCreateResponse createStudent(StudentCreateRequest request) throws SQLException;

	void createStudent(StudentDetailCreateRequest request) throws SQLException;
}
