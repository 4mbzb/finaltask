package com.davydov.finaltask.service;

public class Field<T> {
	private final T value;
	private final String error;
	private final boolean valid;

	public Field(T value, String error, boolean valid) {
		this.value = value;
		this.error = error;
		this.valid = valid;
	}

	public Field(T value) {
		this(value, "", true);
	}

	public Field(T value, String error) {
		this(value, error, false);
	}

	public T getValue() {
		return value;
	}

	public String getError() {
		return error;
	}

	public boolean isValid() {
		return valid;
	}
}
