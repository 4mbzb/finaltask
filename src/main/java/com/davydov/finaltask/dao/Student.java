package com.davydov.finaltask.dao;

import java.util.Optional;

public class Student {
	private final String email;
	private final String password;
	private Long id;
	private Optional<StudentDetail> studentDetail;

	public Student(Long id, String email, String password, StudentDetail studentDetail) {
		this.id = id;
		this.email = email;
		this.password = password;
		this.studentDetail = Optional.ofNullable(studentDetail);
	}

	public Student(Long id, String email, String password) {
		this(id, email, password, null);
	}

	public Student(String email, String password) {
		this(null, email, password, null);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public String getPassword() {
		return password;
	}

	public Optional<StudentDetail> getStudentDetail() {
		return studentDetail;
	}

	public void setStudentDetail(Optional<StudentDetail> studentDetail) {
		this.studentDetail = studentDetail;
	}
}
