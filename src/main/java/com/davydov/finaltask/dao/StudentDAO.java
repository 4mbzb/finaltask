package com.davydov.finaltask.dao;

import java.sql.SQLException;

public interface StudentDAO {
	void saveStudent(Student student) throws SQLException;

	Student findStudentByEmail(String email) throws SQLException;

	Student findStudentById(Long nextStudentId) throws SQLException;
}
