package com.davydov.finaltask.dao;

public class StudentDetail {
	private final Long id;
	private final String name;
	private final String lastName;
	private final String patronymic;
	private final Long universityId;

	public StudentDetail(Long id, String name, String lastName, String patronymic, Long universityId) {
		this.id = id;
		this.name = name;
		this.lastName = lastName;
		this.patronymic = patronymic;
		this.universityId = universityId;
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getLastName() {
		return lastName;
	}

	public String getPatronymic() {
		return patronymic;
	}

	public Long getUniversityId() {
		return universityId;
	}
}
