package com.davydov.finaltask.dao;

public class University {
	private Long id;
	private final String name;
	private final String cityName;
	private final String regionName;
	private final String countryName;

	public University(Long id, String name, String cityName, String regionName, String countryName) {
		this.id = id;
		this.name = name;
		this.cityName = cityName;
		this.regionName = regionName;
		this.countryName = countryName;
	}

	public University(String name, String cityName, String regionName, String countryName) {
		this(null, name, cityName, regionName, countryName);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public String getCityName() {
		return cityName;
	}

	public String getRegionName() {
		return regionName;
	}

	public String getCountryName() {
		return countryName;
	}
}
