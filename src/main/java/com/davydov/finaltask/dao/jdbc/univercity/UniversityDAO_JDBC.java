package com.davydov.finaltask.dao.jdbc.univercity;

import com.davydov.finaltask.dao.University;
import com.davydov.finaltask.dao.UniversityDAO;
import com.davydov.finaltask.dao.jdbc.BaseJDBC_DAO;
import com.davydov.finaltask.dao.jdbc.Credentials;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import static java.lang.String.format;

class UniversityDAO_JDBC extends BaseJDBC_DAO implements UniversityDAO {

    UniversityDAO_JDBC(Credentials credentials) {
        super(credentials);
    }

    @Override
    public University findUniversityByName(String universityName) throws SQLException {
        try (Connection connection = getConnection(); Statement statement = connection.createStatement()) {
            String sql = format("SELECT U.ID," +
                    " U.NAME AS UNIVERSITY_NAME," +
                    " C.NAME AS CITY_NAME," +
                    " R.NAME AS REGION_NAME," +
                    " CTRY.NAME AS COUNTRY_NAME" +
                    " FROM UNIVERSITY U" +
                    " JOIN CITY C" +
                    " ON U.CITY_ID = C.ID" +
                    " JOIN REGION R" +
                    " ON C.REGION_ID = R.ID" +
                    " JOIN COUNTRY CTRY" +
                    " ON R.COUNTRY_ID = CTRY.ID" +
                    " WHERE U.NAME='%s'", universityName
            );

            ResultSet resultSet = statement.executeQuery(sql);

            List<University> students = new ArrayList<>();
            while (resultSet.next()) {
                students.add(convert(resultSet));
            }

            resultSet.close();
            if (students.isEmpty())
                return null;
            else
                return students.get(0);
        }
    }

    @Override
    protected University convert(ResultSet resultSet) throws SQLException {
        return new University(
                resultSet.getLong("ID"),
                resultSet.getString("UNIVERSITY_NAME"),
                resultSet.getString("CITY_NAME"),
                resultSet.getString("REGION_NAME"),
                resultSet.getString("COUNTRY_NAME")
        );
    }
}
