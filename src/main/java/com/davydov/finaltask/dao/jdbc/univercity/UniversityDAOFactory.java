package com.davydov.finaltask.dao.jdbc.univercity;

import com.davydov.finaltask.dao.UniversityDAO;
import com.davydov.finaltask.util.ResourcesUtil;

import java.io.IOException;

public class UniversityDAOFactory {
	public static UniversityDAO getUniversityDAO() throws IOException {
		return new UniversityDAO_JDBC(ResourcesUtil.getDatabaseCredentials());
	}
}
