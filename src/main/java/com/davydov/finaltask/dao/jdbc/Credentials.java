package com.davydov.finaltask.dao.jdbc;

public class Credentials {
	private final String url;
	private final String login;
	private final String password;

	public Credentials(String url, String login, String password) {
		this.url = url;
		this.login = login;
		this.password = password;
	}

	public String getUrl() {
		return url;
	}

	public String getLogin() {
		return login;
	}

	public String getPassword() {
		return password;
	}
}
