package com.davydov.finaltask.dao.jdbc.student;

import com.davydov.finaltask.dao.StudentDAO;
import com.davydov.finaltask.util.ResourcesUtil;

import java.io.IOException;

public class StudentDAOFactory {
    public static StudentDAO getStudentDAO() throws IOException {
        return new StudentDAO_JDBC(ResourcesUtil.getDatabaseCredentials());
    }
}
