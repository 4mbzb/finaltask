package com.davydov.finaltask.dao.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public abstract class BaseJDBC_DAO {
	private final String url;
	private final String login;
	private final String password;

	protected BaseJDBC_DAO(Credentials databaseCredentials) {
		url = databaseCredentials.getUrl();
		login = databaseCredentials.getLogin();
		password = databaseCredentials.getPassword();
	}

	protected Connection getConnection() throws SQLException {
		return DriverManager.getConnection(url, login, password);
	}

	protected abstract Object convert(ResultSet resultSet) throws SQLException;
}
