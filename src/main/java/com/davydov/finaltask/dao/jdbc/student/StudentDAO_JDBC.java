package com.davydov.finaltask.dao.jdbc.student;

import com.davydov.finaltask.dao.Student;
import com.davydov.finaltask.dao.StudentDAO;
import com.davydov.finaltask.dao.StudentDetail;
import com.davydov.finaltask.dao.jdbc.BaseJDBC_DAO;
import com.davydov.finaltask.dao.jdbc.Credentials;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static java.lang.String.format;

class StudentDAO_JDBC extends BaseJDBC_DAO implements StudentDAO {

    StudentDAO_JDBC(Credentials databaseCredentials) {
        super(databaseCredentials);
    }

    @Override
    public void saveStudent(Student student) throws SQLException {
        try (Connection conn = getConnection();
             PreparedStatement preparedStatement = conn.prepareStatement(
                     "INSERT INTO STUDENT(EMAIL, PASSWORD)  VALUES (?, ?)", Statement.RETURN_GENERATED_KEYS
             )
        ) {
            preparedStatement.setString(1, student.getEmail());
            preparedStatement.setString(2, student.getPassword());
            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();

            if (resultSet.next()) {
                student.setId(resultSet.getLong(1));
            } else {
                throw new SQLException("Couldn't receive id for student");
            }

            if (student.getStudentDetail().isPresent()) {
                Statement statement = conn.createStatement();
                StudentDetail studentDetail = student.getStudentDetail().get();
                String sql = format("INSERT INTO STUDENT_DETAIL (ID, NAME, LAST_NAME, PATRONYMIC, UNIVERSITY_ID) " +
                                "VALUES (%d, '%s', '%s', '%s', %d)", studentDetail.getId(), studentDetail.getName(),
                        studentDetail.getLastName(), studentDetail.getPatronymic(), studentDetail.getUniversityId());

                statement.execute(sql);
            }

            conn.commit();
        }
    }

    @Override
    public Student findStudentByEmail(String email) throws SQLException {
        try (Connection conn = getConnection(); Statement statement = conn.createStatement()) {
            String query = format("SELECT ID, EMAIL, PASSWORD FROM STUDENT WHERE EMAIL = '%s'", email);
            ResultSet resultSet = statement.executeQuery(query);

            List<Student> students = new ArrayList<>();
            while (resultSet.next()) {
                students.add(convert(resultSet));
            }

            resultSet.close();
            if (students.isEmpty())
                return null;
            else
                return students.get(0);
        }
    }

    @Override
    public Student findStudentById(Long studentId) throws SQLException {
        try (Connection conn = getConnection(); Statement statement = conn.createStatement()) {
            String query = format("SELECT S.ID AS SID, EMAIL, PASSWORD, " +
                    "SD.ID AS SDID, NAME, LAST_NAME, PATRONYMIC, UNIVERSITY_ID " +
                    "FROM STUDENT S " +
                    "LEFT JOIN STUDENT_DETAIL SD " +
                    "ON S.ID = SD.ID " +
                    "WHERE S.ID = %d", studentId);
            ResultSet resultSet = statement.executeQuery(query);

            List<Student> students = new ArrayList<>();
            while (resultSet.next()) {
                students.add(convert(resultSet));
            }

            resultSet.close();
            if (students.isEmpty())
                return null;
            else
                return students.get(0);
        }
    }

    protected Student convert(ResultSet resultSet) throws SQLException {
        Student student = new Student(
                resultSet.getLong("ID"),
                resultSet.getString("EMAIL"),
                resultSet.getString("PASSWORD")
        );

        if (resultSet.getString("SDID") != null) {
            StudentDetail studentDetail = new StudentDetail(
                    resultSet.getLong("SDID"),
                    resultSet.getString("NAME"),
                    resultSet.getString("LAST_NAME"),
                    resultSet.getString("PATRONYMIC"),
                    resultSet.getLong("UNIVERSITY_ID")
            );

            student.setStudentDetail(Optional.of(studentDetail));
        }

        return student;
    }
}

