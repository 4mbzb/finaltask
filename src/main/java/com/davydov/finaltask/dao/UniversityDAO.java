package com.davydov.finaltask.dao;

import java.sql.SQLException;

public interface UniversityDAO {
	University findUniversityByName(String universityName) throws SQLException;
}
