package com.davydov.finaltask.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static com.davydov.finaltask.util.InputStreamUtil.getInputStreamFrom;

public class PropertyUtil {
	public static String[] loadValues(String propertyPath, String... keys) throws IOException {
		try (InputStream input = getInputStreamFrom(propertyPath)) {
			Properties prop = new Properties();
			prop.load(input);

			String[] values = new String[keys.length];

			for (int i = 0; i < keys.length; i++)
				values[i] = prop.getProperty(keys[i]);

			return values;
		}
	}
}
