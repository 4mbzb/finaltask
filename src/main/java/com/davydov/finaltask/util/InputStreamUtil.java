package com.davydov.finaltask.util;

import java.io.InputStream;

public class InputStreamUtil {
	public static InputStream getInputStreamFrom(String path) {
		return InputStreamUtil.class.getClassLoader().getResourceAsStream(path);
	}
}
