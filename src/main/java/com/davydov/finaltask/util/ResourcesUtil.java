package com.davydov.finaltask.util;

import com.davydov.finaltask.dao.jdbc.Credentials;

import java.io.IOException;

public class ResourcesUtil {
	public static Credentials getDatabaseCredentials() throws IOException {
		String[] credentials = PropertyUtil.loadValues(
				"liquibase/liquibase.properties", "url", "username", "password"
		);
		return new Credentials(
				credentials[0],
				credentials[1],
				credentials[2]
		);
	}
}
