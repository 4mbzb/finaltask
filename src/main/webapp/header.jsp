<header id="header">
    <div class="container">
        <h1>Inspection Board</h1>
        <nav id="nav">
            <ul>
                <li>
                    <a href="#">Home</a>
                </li>
                <li>
                    <a href="#">About</a>
                </li>
                <li>
                    <a href="#">Services</a>
                </li>
                <li>
                    <a href="login.jsp">Login</a>
                </li>
                <li>
                    <a href="registration.jsp">Registration</a>
                </li>
            </ul>
        </nav>
    </div>
</header>