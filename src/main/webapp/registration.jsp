<%--
  Created by IntelliJ IDEA.
  User: Владимир
  Date: 08.09.2017
  Time: 23:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="com.davydov.finaltask.servlets.registration.RegistrationField" %>
<html>
<head>
    <title>Registration</title>
</head>

<body>
<jsp:useBean id="requestDto" class="com.davydov.finaltask.servlets.registration.RegistrationRequestDto"/>
<form name="myForm" action="registration" method="post">
    <table border="1">
        <thead>
        <tr>
            <th colspan="2">Форма регистрации</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>Фамилия Имя Отчество</td>
            <td><input type="text" name="fio" value="" size="50"></td>
        </tr>
        <tr>
            <td>E-Mail</td>
            <td><input type="text" name="<%= RegistrationField.EMAIL%>" value="" size="50"></td>
        </tr>
        <tr>
            <td>Пароль</td>
            <td><input type="password" name="pswd" value="" size="50"></td>
        </tr>
        <tr>
            <td>Город</td>
            <td><input type="text" name="ct" value="" size="50"></td>
        </tr>
        <tr>
            <td>Область</td>
            <td><input type="text" name="rgn" value="" size="50"></td>
        </tr>
        <tr>
            <td>Учебное заведение</td>
            <td><input type="text" name="ei" value="" size="50"></td>
        </tr>
        <tr>
            <td>Дата Рождения</td>
            <td><input type="text" name="birthday" value="DD/MM/YYYY" size="50"></td>
        </tr>
        </tbody>
    </table>
    <br>
    <input type="submit" value="Зарегистрироваться" name="submit">
</form>
</body>
</html>
