<%--
  Created by IntelliJ IDEA.
  User: Владимир
  Date: 09.09.2017
  Time: 0:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Getting Parametrs</title>
</head>
<body>
<%
    request.setCharacterEncoding("UTF-8");
    String nameOfUser = request.getParameter("fio");
    String email = request.getParameter("email");
    String password = request.getParameter("pswd");
    String city = request.getParameter("ct");
    String region = request.getParameter("rgn");
    String educationalInstitution = request.getParameter("ei");
    String birthDay = request.getParameter("birthday");
%>
<table border="1">
    <thead>
    <tr>
        <th colspan="2">Данные</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>Фамилия Имя Отчество</td>
        <td><%=nameOfUser %></td>
    </tr>
    <tr>
        <td>E-Mail</td>
        <td><%=email %></td>
    </tr>
    <tr>
        <td>Пароль</td>
        <td><%=password %></td>
    </tr>
    <tr>
        <td>Город</td>
        <td><%=city %></td>
    </tr>
    <tr>
        <td>Область</td>
        <td><%=region %></td>
    </tr>
    <tr>
        <td>Учебное заведение</td>
        <td><%=educationalInstitution %></td>
    </tr>
    <tr>
        <td>Дата Рождения</td>
        <td><%=birthDay %></td>
    </tr>
    </tbody>
</table>
</body>
</html>
